package datastructure;

import cellular.CellState;
import java.util.Arrays;

public class CellGrid implements IGrid {
	private int rows;
	private int columns;
	private CellState grid[][];

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows=rows;
		this.columns=columns;
    	grid = new CellState[rows][columns];
    	Arrays.stream(grid).forEach(var -> Arrays.fill(var, initialState));
	}
    
    private CellGrid(int rows, int columns) {
		this.rows=rows;
		this.columns=columns;
    	grid = new CellState[rows][columns];
    	
	}
    
    @Override
    public IGrid copy() {
    	CellGrid copy = new CellGrid(this.rows, this.columns);
    	
    	for (int i=0; i<this.rows; i++) {
    		copy.grid[i] = Arrays.copyOf(this.grid[i], this.columns);
    	}
    	
    	return copy;
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if (row<0 || row>this.rows || column<0 || column>this.columns) {
    		throw new IndexOutOfBoundsException();
    	}
        this.grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
    	if (row<0 || row>this.rows || column<0 || column>this.columns) {
    		throw new IndexOutOfBoundsException();
    	}
        return this.grid[row][column];
    }


    
}
