package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		
		
		for (int y = 0; y < this.numberOfRows(); y++) {
			for (int x = 0; x < this.numberOfColumns(); x++) {
				this.getNextCell(y, x);
				nextGeneration.set(y, x, this.getNextCell(y, x));
			}
		}
		
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int count = this.countNeighbors(row, col, CellState.ALIVE);
		if (this.state(row, col)==2) {
			return CellState.DYING;
		}
		else if (this.state(row, col)==1) {
			return CellState.DEAD;
		}
		else if (this.state(row, col)==0 && count==2) {
			return CellState.ALIVE;
		}
		else if (this.state(row, col)==0 && count!=2) {
			return CellState.DEAD;
		}
		else throw new IllegalArgumentException("variable 'count' must be an integer.");
	}
	
	private int state(int row, int col) {
		if (this.getCellState(row, col).equals(CellState.ALIVE)) {
			return 2;
		}
		else if (this.getCellState(row, col).equals(CellState.DYING)) {
			return 1;
		}
		else return 0;
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}
	
	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int[][] hello = {{row-1, col-1}, {row, col-1}, {row+1, col-1}, {row-1, col}, {row+1, col}, {row-1, col+1}, {row, col+1},{row+1, col+1}};
		int count = 0;
		for (int i[] : hello) {
			try {
				if (this.getCellState(i[0], i[1]).equals(state)) {
					count++;
				}
			} catch (IndexOutOfBoundsException e) {
				//CellState.get() gives this exception if the index is out of bounds. This part of the code
				//uses that exception to save lines having to check.
				;
			}
		}
		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}

}
